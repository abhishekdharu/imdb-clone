Base URL: https://imdb-clone06.herokuapp.com/

**Movie Search APIs**

Type: GET

Get movies by name

https://imdb-clone06.herokuapp.com/search?name=The Wizard of Oz

Get movies which has a substring "The" in their name

https://imdb-clone06.herokuapp.com/search?name=The

Get movies by director

https://imdb-clone06.herokuapp.com/search?director=Giovanni Pastrone

Get movies which are greater than a imdb score

https://imdb-clone06.herokuapp.com/search?imdb_score=8

Get movies which are greater than a popularity score

https://imdb-clone06.herokuapp.com/search?popular_score=8

Get movies of a particular genre(s)(multiple genre comma seperated)

https://imdb-clone06.herokuapp.com/search?genre=Adventure

https://imdb-clone06.herokuapp.com/search?genre=Adventure,Drama

Get movies of a particular genre(s) and imdb_ratings greater than 8

https://imdb-clone06.herokuapp.com/search?genre=Adventure&imdb_score=8

https://imdb-clone06.herokuapp.com/search?genre=Adventure,Drama&imdb_score=8

Get movies of a director with imdb_ratings greater than 8

https://imdb-clone06.herokuapp.com/search?director=John Ford&imdb_score=8

**Admin APIs**

Add a movie to the database<br>
Type: POST

headers = { "user_id": 1}  #User validated as admin

body = {

"99popularity":83,

"director":"Victor Fleming",

"genre":["Adventure"," Family"," Fantasy"," Musical"],

"imdb_score":8.3,

"name":"Dhoom"

`}

https://imdb-clone06.herokuapp.com/add_movie

Edit a movie in the database

Type: PUT

headers = { "user_id": 1}  #User validated as admin

body = {

"imdb_score":9.3

`}

https://imdb-clone06.herokuapp.com/update_movie/Dhoom

Delete a movie from the database

Type: DELETE

headers = { "user_id": 1}  #User validated as admin

https://imdb-clone06.herokuapp.com/delete_movie/Dhoom

**Scalibility**

1. Most of the users will search for the most famous movies, directors or movies having top imdb ratings. Such highly searched movie data could be stored in a cache like Redis so that we do not need to query the database.
2. As most of the queries to the DB will be read operations, we can direct them to secondary DBs, thereby having an uniform load on DB cluster and increasing the throughput of the system.
3. Can create text indexes on columns like "name", "director" thereby improving the efficiency of queries.
   We can also migrate to DBs like ElasticSearch which are much more efficient for text-based queries
4. Using async/await paradigm can increase the concurrency of the application.
