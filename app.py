from functools import wraps
from flask import Flask, jsonify, request, g, make_response
from jsonschema import ValidationError
from flask_expects_json import expects_json
from database_connection import db

app = Flask(__name__)

insert_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'director': {'type': 'string'},
        'imdb_score': {'type': 'number',  'minimum': 0, 'maximum': 10},
        '99popularity': {'type': 'number', 'minimum': 0, 'maximum': 100},
        'genre': {'type': 'array'}

    },
    'required': ['name', 'director', 'imdb_score', '99popularity', 'genre']
}


update_schema = {
    'type': 'object',
    'properties': {
        'director': {'type': 'string'},
        'imdb_score': {'type': 'number',  'minimum': 0, 'maximum': 10},
        '99popularity': {'type': 'number', 'minimum': 0, 'maximum': 100},
        'genre': {'type': 'array'}

    },
    "additionalProperties": False
}

def admin_login(func):
    @wraps(func)
    def inner(*args, **kwargs):
        user_id = request.headers["user_id"]
        
        user_data = db.users.find_one(
            {"user_id": int(user_id)}, {"_id": 0}
        )
        if user_data and user_data.get('role') == 'admin':
            return func(*args, **kwargs)
        else:
            return 'Need to be admin', 401
    return inner


@app.route("/search", methods=["GET"])
def search():
    """Search movies by various fields(name, director, genre, imdb_score, popularity)"""

    request_data = request.args
    print("request_data", request_data)
    query_dict = {}

    if 'name' in request_data:
        query_dict['name'] = {"$regex": request_data["name"], "$options": "i"}

    if 'director' in request_data:
        query_dict['director'] = {"$regex": request_data["director"], "$options": "i"}

    if 'imdb_score' in request_data:
        query_dict['imdb_score'] = { "$gt": float(request_data["imdb_score"]) }

    if 'popular_score' in request_data:
        query_dict['99popularity'] = {"$gt": int(request_data["popular_score"]) }

    if 'genre' in request_data:
        genre_list = request_data['genre'].split(',')
        query_dict['genre'] = { "$all": genre_list } 

    print(query_dict)

   
    movie_details = list( db.movies.find( query_dict, {"_id": 0} ) )
    return jsonify({"status": bool(movie_details), "data": movie_details})




@app.route("/add_movie", methods=['POST'])
@expects_json(insert_schema)
@admin_login
def add_movie():
    request_data = request.get_json()
    db.movies.insert_one(request_data)
    return f"Movie {request_data['name']} has been added successfully", 201

    
@app.route("/delete_movie/<name>", methods=['DELETE'])
@admin_login
def delete_movie(name):
    
    db.movies.delete_one({'name': name})
    return f"Movie {name} deleted Successfully", 200

@app.route("/update_movie/<name>", methods=['PUT'])
@expects_json(update_schema)
@admin_login
def update_movie(name):
    request_data = request.get_json()
    #print(request_data)
    db.movies.update_one({'name': name}, {'$set': request_data})
    return f"Movie {name} updated Successfully", 200

@app.errorhandler(400)
def bad_request(error):
    if isinstance(error.description, ValidationError):
        original_error = error.description
        return make_response(jsonify({'error': original_error.message}), 400)
    
    return error

if __name__ == "__main__":

    app.run(host="0.0.0.0")
