import pytest
import sys, os, json
from unittest import mock
from pymongo import MongoClient

sys.path.append(os.getcwd())

from app import app
from database_connection import db

@pytest.fixture
def test_client(autouse=True):
    app.testing = True
    testing_client = app.test_client()
    
    yield testing_client

@pytest.fixture
def mongo_db():
    yield db


@pytest.mark.parametrize(
    "name,status,no_of_results",
    [
        ('The Wizard of Oz', True, 2),
        ('Dhoom', False, 0)
    ]
)

def test_search_movies_by_name(test_client, name, status, no_of_results):
    args = {'name': name}
    r = test_client.get('/search', query_string = args)
    assert r.status_code == 200

    response = json.loads(r.get_data())
    assert len(response['data']) == no_of_results
    assert response['status'] == status

@pytest.mark.parametrize(
    "name,status,no_of_results",
    [
        ('Giovanni Pastrone', True, 1),
        ('Rajkumar Hirani', False, 0)
    ]
)

def test_search_movies_by_directors(test_client, name, status, no_of_results):
    args = {'director': name}
    r = test_client.get('/search', query_string = args)
    assert r.status_code == 200

    response = json.loads(r.get_data())
    assert len(response['data']) == no_of_results
    assert response['status'] == status



@pytest.mark.parametrize(
    "imdb_score,no_of_results",
    [
        ('8.4', 48)
    ]
)
def test_search_movies_by_imdb_score(test_client, imdb_score, no_of_results):
    args = {'imdb_score': imdb_score}
    r = test_client.get('/search', query_string = args)
    assert r.status_code == 200

    response = json.loads(r.get_data())
    assert len(response['data']) == no_of_results
   

@pytest.mark.parametrize(
    "imdb_score,director,no_of_results",
    [
        ('8.4', 'Alfred Hitchco', 4)
    ]
)
def test_search_movies_by_imdb_score_and_director(test_client, imdb_score, director, no_of_results):
    args = {'imdb_score': imdb_score, 'director': director }
    r = test_client.get('/search', query_string = args)
    assert r.status_code == 200

    response = json.loads(r.get_data())
    assert len(response['data']) == no_of_results



@pytest.mark.parametrize(
    "genre,no_of_results",
    [
        ('Adventure,Drama', 25)
        
    ]
)

def test_search_movies_by_genre(test_client, genre, no_of_results):
    print(genre)
    args = {'genre': genre}
    r = test_client.get('/search', query_string = args)
    assert r.status_code == 200

    response = json.loads(r.get_data())
    assert len(response['data']) == no_of_results

@pytest.mark.parametrize(
    "genre,imdb_score,no_of_results",
    [
        ('Adventure,Drama', 7.0, 21)
        
    ]
)

def test_search_movies_by_genre_and_imdb_score(test_client, genre, imdb_score, no_of_results):
    print(genre)
    args = {'genre': genre, 'imdb_score': imdb_score}
    r = test_client.get('/search', query_string = args)
    assert r.status_code == 200

    response = json.loads(r.get_data())
    print(response['data'])
    assert len(response['data']) == no_of_results


def test_add_movie(test_client):
    headers = {'user_id': 1}
    body = {"99popularity":83,"director":"Victor Fleming","genre":["Adventure"," Family"," Fantasy"," Musical"],
            "imdb_score":8.3,"name":"Dhoom"}
    r = test_client.post('/add_movie', json=body, headers=headers)
    #print(r.json())
    assert r.status_code == 201
    
    db.movies.delete_one(body)

def test_delete_movie(test_client):
    headers = {'user_id': 1}
    doc = {"99popularity":83,"director":"Victor Fleming","genre":["Adventure"," Family"," Fantasy"," Musical"],
            "imdb_score":8.3,"name":"Dhoom"}
    db.movies.insert_one(doc)
    #body = {'name': 'Dhoom', 'director': 'avc'}
    r = test_client.delete('/delete_movie/Dhoom', headers=headers)
    #print(r.json())
    assert db.movies.find_one({'name': 'Dhoom'}) is None
    assert r.status_code == 200

def test_update_movie(test_client):
    headers = {'user_id': 1}
    doc = {"99popularity":83,"director":"Victor Fleming","genre":["Adventure"," Family"," Fantasy"," Musical"],
            "imdb_score":8.3,"name":"Dhoom"}
    db.movies.insert_one(doc)
    body = {'director': 'ABC'}
    r = test_client.put('/update_movie/Dhoom', json=body, headers=headers)
    
    assert r.status_code == 200
    assert db.movies.find_one({'name': 'Dhoom'})['director'] == 'ABC'
    db.movies.delete_many({'name': 'Dhoom'})

def test_invalid_user(test_client):
    headers = {'user_id': 5}
    
    body = {'director': 'ABC'}
    r = test_client.put('/update_movie/abc', json=body, headers=headers)
    #print(r.json())
    assert r.status_code == 401
    
def test_invalid_schema(test_client):
    headers = {'user_id': 1}
    body = {"director":"Victor Fleming","genre":["Adventure"," Family"," Fantasy"," Musical"],
            "imdb_score":8.3,"name":"Dhoom"}
    r = test_client.put('/update_movie/Dhoom', json=body, headers=headers)
    #print(r.json())
    assert r.status_code == 400
    
    

